﻿#include<iostream>
#include<vector>


bool is_leaf(int node_index, const std::vector<int> &visited, const std::vector<std::vector<int>> &graph)
{
	for (auto adj_node : graph[node_index])
		// Вершина будет листом только тогда, когда все связанные с ней вершины уже обошли
		// (т.е. они вляются ее предками в силу особенности обхода дерева)
		if (visited[adj_node] != 1 || node_index == 1)
			return false;

	return true;
}


void dfs(
		const int node_index,
		int cnt,
		const int max_cats,
		int &answ,
		std::vector<int>& visited,
		const std::vector<int>& cats,
		const std::vector<std::vector<int>>& graph
)
{
	if (visited[node_index] != 0)
		return;

	visited[node_index] = 1;

	cnt = (cnt + 1) * static_cast<int>(cats[node_index] != 0);

	if (cnt > max_cats)
		return;

	answ += static_cast<int>(is_leaf(node_index, visited, graph));
	
	for (auto child_node : graph[node_index])
		dfs(child_node, cnt, max_cats, answ, visited, cats, graph);
}


int main()
{
	int maxn = 1e5 + 1;
	int answ = 0;
	auto visited = std::vector<int>(maxn, 0);
	auto cats = std::vector<int>(maxn, 0);
	auto graph = std::vector<std::vector<int>>(maxn, std::vector<int>());

	int num_nodes, max_cats;
	std::cin >> num_nodes >> max_cats;

	for (int i = 1; i < num_nodes + 1; i++)
		std::cin >> cats[i];
	
	int x, y;
	for (int __ = 0; __ < num_nodes - 1; __++)
	{
		std::cin >> x >> y;
		graph[x].push_back(y);
		graph[y].push_back(x);
	}

	dfs(1, 0, max_cats, answ, visited, cats, graph);

	std::cout << answ << std::endl;

	return 0;
}
