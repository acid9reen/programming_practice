﻿#include<iostream>
#include<vector>
#include<unordered_map>

// В качестве общего замечания: просто с int завалилось на codeforces, поэтому long long
int main()
{
	long long n_elems{};
	std::cin >> n_elems;

	// Счетчик элементов
	auto cnts = std::unordered_map<long long, long long>();
	long long max_elem{0LL};

	long long cur_elem{};
	for (long long i = 0LL; i < n_elems; i++)
	{	
		std::cin >> cur_elem;
		// Дефолтное поведение, если такого ключа нет, то инициализирует 0-ём
		cnts[cur_elem]++;

		// Простой поиск максимума
		if (cur_elem > max_elem)
			max_elem = cur_elem;
	}

	// Вообще можно обойтись и без массива, достатчоно хранить результаты двух предыдущих итераций
	// Ещё одно наблюдение, тут между обычным массивом и вектором не было разницы по времени на codeforces
	auto dp = std::vector<long long>(max_elem + 1LL, 0LL);

	// База
	dp[0] = 0LL;
	dp[1] = cnts[1];
	
	// Дин прог
	for (long long i = 2LL; i < max_elem + 1LL; i++)
		dp[i] = std::max(dp[i - 1], dp[i - 2LL] + cnts[i] * i);

	std::cout << dp[max_elem] << std::endl;
}
